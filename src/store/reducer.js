import {
    ADD_COMPANY,
    ADD_EMPLOYEE,
    EDIT_EMPLOYEE_NAME,
    EDIT_EMPLOYEE_AUTHOR,
    REMOVE_EMPLOYEE,
    DOWNLOAD_EMPLOYEES_DATA,
    MOVE_EMPLOYEE_LEFT,
    MOVE_EMPLOYEE_RIGHT
} from './actions';

const initialState = {
    companys: []
};

export default function reducer(state=initialState, {type, payload}) {
    let employeeToMove = null;

    switch(type) {
    case ADD_COMPANY:
        return {
            ...state,
            companys: [
                ...state.companys, payload
            ]
        };
    case ADD_EMPLOYEE:
        return {
            ...state,
            companys: state.companys.map((employeeArr, index) => (
                index === payload.employeeArrId ? {
                    ...employeeArr,
                    employees: [...employeeArr.employees, payload.employee]
                }
                : employeeArr
            ))
        };
    case EDIT_EMPLOYEE_NAME:
        return {
            ...state,
            companys: state.companys.map((employeeArr, index) => (
                index === payload.employeeArrId ? {
                    ...employeeArr,
                    employees: employeeArr.employees.map((employee, indexEmployee) => (
                        indexEmployee === payload.employeeId ? {
                            ...employee,
                            name: payload.newName
                        }
                        : employee
                    ))
                }
                : employeeArr
            ))
        };
    case EDIT_EMPLOYEE_AUTHOR:
        return {
            ...state,
            companys: state.companys.map((employeeArr, index) => (
                index === payload.employeeArrId ? {
                    ...employeeArr,
                    employees: employeeArr.employees.map((employee, indexEmployee) => (
                        indexEmployee === payload.employeeId ? {
                            ...employee,
                            author: payload.newAuthor
                        }
                        : employee
                    ))
                }
                : employeeArr
            ))
        };
    case REMOVE_EMPLOYEE:
        return {
            ...state,
            companys: state.companys.map((employeeArr, index) => (
                index === payload.employeeArrId ? {
                    ...employeeArr,
                    employees: employeeArr.employees.filter((employee, employeeIndex) => (employeeIndex !== payload.employeeId))
                }
                : employeeArr
            ))
        };
    case DOWNLOAD_EMPLOYEES_DATA:
        return {
            ...state,
            companys: payload
        }
    case MOVE_EMPLOYEE_LEFT:
        employeeToMove = state.companys[payload.employeeArrId].employees[payload.employeeId];

        return {
            ...state,
            companys: state.companys.map((employeeArr, index) => {
                if (index === payload.employeeArrId) {
                    return {
                        ...employeeArr,
                        employees: employeeArr.employees.filter((employee, employeeIndex) => (employeeIndex !== payload.employeeId))
                    };
                }
                if (index === payload.employeeArrId - 1) {
                    return {
                        ...employeeArr,
                        employees: [...employeeArr.employees, employeeToMove]
                    };
                }
                return employeeArr;
            })
        };
    case MOVE_EMPLOYEE_RIGHT:
        employeeToMove = state.companys[payload.employeeArrId].employees[payload.employeeId];

        return {
            ...state,
            companys: state.companys.map((employeeArr, index) => {
                if (index === payload.employeeArrId) {
                    return {
                        ...employeeArr,
                        employees: employeeArr.employees.filter((employee, employeeIndex) => (employeeIndex !== payload.employeeId))
                    };
                }
                if (index === payload.employeeArrId + 1) {
                    return {
                        ...employeeArr,
                        employees: [...employeeArr.employees, employeeToMove]
                    };
                }
                return employeeArr;
            })
        };
    default:
        return state;
    }
};
