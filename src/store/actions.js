const ADD_COMPANY = 'ADD_COMPANY';
const ADD_EMPLOYEE = 'ADD_EMPLOYEE';
const EDIT_EMPLOYEE_NAME = 'EDIT_EMPLOYEE_NAME';
const EDIT_EMPLOYEE_AUTHOR = 'EDIT_EMPLOYEE_AUTHOR';
const REMOVE_EMPLOYEE = 'REMOVE_EMPLOYEE';
const DOWNLOAD_EMPLOYEES_DATA = 'DOWNLOAD_EMPLOYEES_DATA';
const MOVE_EMPLOYEE_LEFT = 'MOVE_EMPLOYEE_LEFT';
const MOVE_EMPLOYEE_RIGHT = 'MOVE_EMPLOYEE_RIGHT';


const addCompanyAction = (employeeArr) => ({
    type: ADD_COMPANY,
    payload: employeeArr
});

const addEmployeeAction = ({ employee, employeeArrId }) => ({
    type: ADD_EMPLOYEE,
    payload: { employee, employeeArrId }
});

const editEmployeeNameAction = ({ employeeId, employeeArrId, newName }) => ({
    type: EDIT_EMPLOYEE_NAME,
    payload: { employeeId, employeeArrId, newName }
});

const editEmployeeAuthorAction = ({ employeeId, employeeArrId, newAuthor }) => ({
    type: EDIT_EMPLOYEE_AUTHOR,
    payload: { employeeId, employeeArrId, newAuthor }
});

const removeEmployeeAction = ({ employeeId, employeeArrId }) => ({
    type: REMOVE_EMPLOYEE,
    payload: { employeeId, employeeArrId }
});

const downloadEmployeesDataAction = (companys) => ({
    type: DOWNLOAD_EMPLOYEES_DATA,
    payload: companys
});

const moveEmployeeLeftAction = ({ employeeId, employeeArrId }) => ({
    type: MOVE_EMPLOYEE_LEFT,
    payload: { employeeId, employeeArrId }
});

const moveEmployeeRightAction = ({ employeeId, employeeArrId  }) => ({
    type: MOVE_EMPLOYEE_RIGHT,
    payload: { employeeId, employeeArrId }
});

export {
    ADD_COMPANY,
    ADD_EMPLOYEE,
    EDIT_EMPLOYEE_NAME,
    EDIT_EMPLOYEE_AUTHOR,
    REMOVE_EMPLOYEE,
    DOWNLOAD_EMPLOYEES_DATA,
    MOVE_EMPLOYEE_LEFT,
    MOVE_EMPLOYEE_RIGHT,
    addCompanyAction,
    addEmployeeAction,
    editEmployeeNameAction,
    editEmployeeAuthorAction,
    removeEmployeeAction,
    downloadEmployeesDataAction,
    moveEmployeeLeftAction,
    moveEmployeeRightAction
};
