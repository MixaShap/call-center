import { Fragment, PureComponent } from 'react';
import { connect } from 'react-redux';

import Company from '../Company/Company';

import { getCompanys, addCompany } from '../../model/model';

import { downloadEmployeesDataAction, addCompanyAction } from '../../store/actions';
import './App.css';

class App extends PureComponent {

    state = {
        isInputActive: false,
        inputValue: ''
    };

    async componentDidMount() {
        const companys = await getCompanys();
        this.props.downloadEmployeesDataDispatch(companys);
    }

    inputCompany = () => {
        this.setState({
            isInputActive: true
        });
    }

    onKeyDown = async (event) => {
        if (event.key === 'Escape') {
          this.setState({
            isInputState: false,
            inputValue: ''
          });
        }
    
        if (event.key === 'Enter') {
            const employeeArrName = this.state.inputValue;

            this.setState({
                isInputState: false,
                inputValue: ''
            })
            const employeeArr = { name: employeeArrName, employees: [] };
            await addCompany(employeeArr);
            this.props.addCompanyDispatch(employeeArr);
        }
    }
    
    onInputChange = (event) => {
        this.setState({
            inputValue: event.target.value
        });
    }

    render() {
        const { inputValue, isInputActive } = this.state;

        return (
            <Fragment>
                <header id="main-header">
                    Organizations clients call numbers
                </header>
                <main id="main-container">
                    {this.props.companys.map((employeeArr, index) => (
                        <Company key={`employeearr-${index}`} employeeArrId={index}/>
                    ))}
                    <div className="employeearr">
                    {isInputActive && <input
                        type="text"
                        id="add-employeearr-input"
                        placeholder="Имя организации"
                        value={inputValue}
                        onChange={this.onInputChange}
                        onKeyDown={this.onKeyDown}
                    />}
                    {!isInputActive && <header className="employeearr-name" onClick={this.inputCompany}>
                        Добавить организацию
                    </header>}
                    </div>
                </main>
            </Fragment>
        );
    }
}

const mapStateToProps = ({ companys }) => ({ companys });

const mapDispatchToProps = dispatch => ({
    addCompanyDispatch: (employeeArr) => dispatch(addCompanyAction(employeeArr)),
    downloadEmployeesDataDispatch: (companys) => dispatch(downloadEmployeesDataAction(companys)),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(App);
