import { PureComponent } from 'react';
import { connect } from 'react-redux';
import './Employee.css';

import { editEmployee, removeEmployee, moveEmployee } from '../../model/model';

import { 
    editEmployeeNameAction,
    editEmployeeAuthorAction,
    removeEmployeeAction,
    moveEmployeeLeftAction,
    moveEmployeeRightAction
} from '../../store/actions';


class Employee extends PureComponent {

    moveLeft = async () => {
        const moveData = {
            employeeId: this.props.employeeId,
            employeeArrId: this.props.employeeArrId
        };
        await moveEmployee({
            ...moveData,
            destShelfId: moveData.employeeArrId - 1
        });
        this.props.moveEmployeeLeftDispatch(moveData);
    }

    moveRight = async () => {
        const moveData = {
            employeeId: this.props.employeeId,
            employeeArrId: this.props.employeeArrId
        };
        await moveEmployee({
            ...moveData,
            destShelfId: moveData.employeeArrId + 1
        });
        this.props.moveEmployeeRightDispatch(moveData);
    }

    onRemove = async () => {
        const ok = window.confirm('Удалить запись о сотруднике?');
        if (!ok) {
            return;
        }

        const removeData = {
            employeeId: this.props.employeeId,
            employeeArrId: this.props.employeeArrId
        };
        await removeEmployee(removeData);
        this.props.removeEmployeeDispatch(removeData);
    }

    onAuthorEdit = async () => {
        let newAuthor = prompt('Введите новый номер телефона');
        if (!newAuthor || !newAuthor.trim()) {
            alert('Неверный номер телефона');
            return;
        }

        newAuthor = newAuthor.trim();

        const employee = this.props.companys[this.props.employeeArrId].employees[this.props.employeeId];
        const employeeEditData = {
            employeeId: this.props.employeeId,
            employeeArrId: this.props.employeeArrId,
            newAuthor: newAuthor
        };
        await editEmployee({
            ...employeeEditData,
            newName: employee.name
        });
        this.props.editEmployeeAuthorDispatch(employeeEditData);
    }

    onNameEdit = async () => {
        let newName = prompt('Введите новоe ФИО сотрудника');
        if (!newName || !newName.trim()) {
            alert('Невозможное ФИО сотрудника');
            return;
        }
        
        newName = newName.trim();

        const employee = this.props.companys[this.props.employeeArrId].employees[this.props.employeeId];
        const employeeEditData = {
            employeeId: this.props.employeeId,
            employeeArrId: this.props.employeeArrId,
            newName: newName,
        };
        await editEmployee({
            ...employeeEditData,
            newAuthor: employee.author
        });
        this.props.editEmployeeNameDispatch(employeeEditData);
    }

    render() {
        const { employeeId, employeeArrId } = this.props;
        const employee = this.props.companys[employeeArrId].employees[employeeId];

        return (
            <div className="employeearr-employee">
                <div className="employeearr-employee-description">
                <div className="employeearr-employee-name">
                    { employee.name }
                </div>
                <div className="employeearr-employee-author">
                    { employee.author }
                </div>
                </div>
                
                <div className="employeearr-employee-controls">
                <div className="employeearr-employee-controls-row">
                    <div className="employeearr-employee-controls-icon left-arrow-icon" onClick={this.moveLeft}></div>
                    <div className="employeearr-employee-controls-icon right-arrow-icon" onClick={this.moveRight}></div>
                </div>
                <div className="employeearr-employee-controls-row">
                    <div className="employeearr-employee-controls-icon delete-icon" onClick={this.onRemove}></div>
                </div>
                <div className="employeearr-employee-controls-row">
                    <div className="employeearr-employee-controls-icon editcust-icon" onClick={this.onAuthorEdit}></div>
                    <div className="employeearr-employee-controls-icon editdesc-icon" onClick={this.onNameEdit}></div>
                </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = ({ companys }) => ({ companys });

const mapDispatchToProps = dispatch => ({
    editEmployeeNameDispatch: ({ employeeId, employeeArrId, newName }) => dispatch(editEmployeeNameAction({ employeeId, employeeArrId, newName })),
    editEmployeeAuthorDispatch: ({ employeeId, employeeArrId, newAuthor }) => dispatch(editEmployeeAuthorAction({ employeeId, employeeArrId, newAuthor })),
    removeEmployeeDispatch: ({ employeeId, employeeArrId }) => dispatch(removeEmployeeAction({ employeeId, employeeArrId })),
    moveEmployeeLeftDispatch: ({ employeeId, employeeArrId }) => dispatch(moveEmployeeLeftAction({ employeeId, employeeArrId })),
    moveEmployeeRightDispatch: ({ employeeId, employeeArrId }) => dispatch(moveEmployeeRightAction({ employeeId, employeeArrId })),
});
  
  
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Employee);
