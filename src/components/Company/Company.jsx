import { PureComponent } from 'react';
import { connect } from 'react-redux';
import './Company.css';

import { addEmployee } from '../../model/model';

import Employee from '../Employee/Employee';
import { addEmployeeAction } from '../../store/actions';


class Company extends PureComponent {

    onEmployeeAdd = async () => {
        let employeeName = prompt('Введите ФИО сотрудника', '');
        if (!employeeName || !employeeName.trim()) {
            alert('Невалидное ФИО сотрудника!');
            return;
        }
        employeeName = employeeName.trim();

        let employeeAuthor = prompt('Укажите номер телефона сотрудника', '').trim();
        if (!employeeAuthor || !employeeAuthor.trim()) {
            alert('Неверный номер телефона!');
            return;
        }

        employeeAuthor = employeeAuthor.trim();
        const newEmployeeData = {
            employee: {
                name: employeeName,
                author: employeeAuthor
            },
            employeeArrId: this.props.employeeArrId
        };

        await addEmployee(newEmployeeData);
        this.props.addEmployeeDispatch(newEmployeeData);
    }

    render() {
        const employeeArrId = this.props.employeeArrId;
        const employeeArr = this.props.companys[employeeArrId];

        return (
        <div className="employeearr">
            <header className="employeearr-name">
                { employeeArr.name }
            </header>
            <div className="employeearr-employees">
                {employeeArr.employees.map((employee, index) => (
                    <Employee key={`employee-${index}`} employeeId={index} employeeArrId={employeeArrId} />
                ))}
            </div>
            <footer className="employeearr-add-task" onClick={this.onEmployeeAdd}>
                Добавить сотрудника
            </footer>
        </div>
        );
    }
}

const mapStateToProps = ({ companys }) => ({ companys });

const mapDispatchToProps = dispatch => ({
    addEmployeeDispatch: ({ employee, employeeArrId }) => dispatch(addEmployeeAction({ employee, employeeArrId })),
});
  
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Company);
