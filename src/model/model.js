const hostname = 'http://localhost:9999';

const getCompanys = async () => {
    const response = await fetch(hostname + '/employeearr', {method: 'GET'});
    if (response.status !== 200) {
        throw new Error(`getCompanys returned ${response.status}`);
    }
    const jsonData = await response.json();
    return jsonData;
};

const addCompany = async (employeeArr) => {
    const response = await fetch(hostname + '/employeearr', {
        method: 'POST', 
        body: JSON.stringify(employeeArr),
        headers: {
            'Content-Type': 'application/json'
        }
    });

    if (response.status !== 200) {
        throw new Error(`addCompany returned ${response.status}`);
    }
    const { info } = await response.json();
    console.log(info);
};

const addEmployee = async ({ employee, employeeArrId }) => {
    const response = await fetch(hostname + `/employeearr/${employeeArrId}/employee`, {
        method: 'POST', 
        body: JSON.stringify(employee),
        headers: {
            'Content-Type': 'application/json'
        }
    });

    console.log(response);

    if (response.status !== 200) {
        throw new Error(`addEmployee returned ${response.status}`);
    }
    const { info } = await response.json();
    console.log(info);
};

const editEmployee = async ({ employeeId, employeeArrId, newName, newAuthor }) => {
    const response = await fetch(hostname + `/employeearr/${employeeArrId}/employee/${employeeId}`, {
        method: 'PATCH', 
        body: JSON.stringify({ newName: newName, newAuthor: newAuthor }), 
        headers: {
            'Content-Type': 'application/json'
        }
    });

    if (response.status !== 200) {
        throw new Error(`editEmployeeName returned ${response.status}`);
    }
    const { info } = await response.json();
    console.log(info);
};

const removeEmployee = async ({ employeeId, employeeArrId }) => {
    const response = await fetch(hostname + `/employeearr/${employeeArrId}/employee/${employeeId}`, {
        method: 'DELETE'
    });

    if (response.status !== 200) {
        throw new Error(`removeEmployee returned ${response.status}`);
    }
    const { info } = await response.json();
    console.log(info);
};

const moveEmployee = async ({ employeeId, employeeArrId, destShelfId }) => {
    const response = await fetch(hostname + `/employeearr/${employeeArrId}`, {
        method: 'PATCH',
        body: JSON.stringify({ employeeId: employeeId, destShelfId: destShelfId }),
        headers: {
            'Content-Type': 'application/json'
        }
    });

    if (response.status !== 200) {
        throw new Error(`removeEmployee returned ${response.status}`);
    }
    const { info } = await response.json();
    console.log(info);
};

export {
    getCompanys,
    addCompany,
    addEmployee,
    editEmployee,
    removeEmployee,
    moveEmployee,
};
