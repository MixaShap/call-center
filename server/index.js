const express = require('express');
const app = express();
const { readData, writeData } = require('./utils');

const port = 9999;
const hostname = 'localhost';

let companys = [];

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.use(express.json());

app.options('/*', (request, response) => {
    response.statusCode = 200;
    response.send('OK');
});

app.get('/employeearr', async (request, response) => {
    companys = await readData();
    response.setHeader('Content-Type', 'application/json');
    response.status(200).json(companys);
});

app.post('/employeearr', async (request, response) => {
    const employeeArr = request.body;
    companys.push(employeeArr);
    await writeData(companys);
    response.status(200).json({info: 'Employee succefully created!'});
});

app.post('/employeearr/:employeeArrId/employee', async (request, response) => {
    const employee = request.body;
    const employeeArrId = Number(request.params.employeeArrId);
    companys[employeeArrId].employees.push(employee);
    await writeData(companys);
    response.status(200).json({info: 'Employee succefully created!'});
});

app.patch('/employeearr/:employeeArrId/employee/:employeeId', async (request, response) => {
    const { newName, newAuthor } = request.body;
    const employeeArrId = Number(request.params.employeeArrId);
    const employeeId = Number(request.params.employeeId);

    companys[employeeArrId].employees[employeeId].name = newName;
    companys[employeeArrId].employees[employeeId].author = newAuthor;

    await writeData(companys);
    response.status(200).json({info: 'Employee succefully changed!'});
});

app.delete('/employeearr/:employeeArrId/employee/:employeeId', async (request, response) => {
    const employeeArrId = Number(request.params.employeeArrId);
    const employeeId = Number(request.params.employeeId);

    companys[employeeArrId].employees.splice(employeeId, 1);

    await writeData(companys);
    response.status(200).json({info: 'Employee succefully deleted!'});
});

app.patch('/employeearr/:employeeArrId', async (request, response) => {
    const employeeArrId = Number(request.params.employeeArrId);
    const { employeeId, destShelfId } = request.body;

    const employeeToMove =  companys[employeeArrId].employees.splice(employeeId, 1);
    companys[destShelfId].employees.push(employeeToMove);

    await writeData(companys);
    response.status(200).json({info: 'Employee succefully moved!'});
});

app.listen(port, hostname, (error) => {
    if (error) {
        console.error(error);
    }
});
